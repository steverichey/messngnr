package objects;

import flixel.tile.FlxTilemap;
import flixel.tile.FlxBaseTilemap;
import openfl.Assets;

class Level extends FlxTilemap
{
	public static var levelWidth:Int = 0;
	public static var levelHeight:Int = 0;
	
	public function new()
	{
		super();
		
		loadMap(Assets.getText("maps/" + Global.currentLevel + ".csv"), "images/tiles.png", 16, 16, FlxTilemapAutoTiling.OFF, 1, 1);
		
		levelWidth = Std.int(width);
		levelHeight = Std.int(height);
	}
}