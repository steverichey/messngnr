package objects;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;

class Player extends FlxSprite
{
	inline static private var ACCEL:Int = 400;
	inline static private var JUMP:Int = -200;
	inline static private var GRAVITY:Int = 800;
	inline static private var MAXFALL:Int = 400;
	inline static private var MAXRUN:Int = 200;
	
	public function new()
	{
		super(32);
		
		loadGraphic("images/player.png", true, 16, 16);
		
		animation.add("run", [1, 2, 3], 12);
		animation.add("idle", [0]);
		animation.add("jump", [4]);
		animation.add("fall", [5]);
		
		drag.set(400, 400);
		acceleration.y = GRAVITY;
		maxVelocity.set(MAXRUN, MAXFALL);
		
		setSize(10, 12);
		offset.set(3, 4);
		
		setFacingFlip(FlxObject.LEFT, true, false);
		setFacingFlip(FlxObject.RIGHT, false, false);
	}
	
	override public function update():Void
	{
		if (!ReadingMessage.isReading)
		{
			acceleration.x = getAccelFromInput();
			getVelocityFromInput();
		}
		
		if (velocity.x < 0) { facing = FlxObject.LEFT; }
		if (velocity.x > 0) { facing = FlxObject.RIGHT; }
		
		if (!isTouching(FlxObject.FLOOR))
		{
			if (velocity.y < 0)
			{
				animation.play("jump");
			}
			else
			{
				animation.play("fall");
			}
		}
		else
		{
			if (velocity.x == 0)
			{
				animation.play("idle");
			}
			else
			{
				animation.play("run");
			}
		}
		
		if (x < 0) x = 0;
		if (y < 0) y = 0;
		if (x + width > Level.levelWidth) x = Level.levelWidth - width;
		if (y + height > Level.levelHeight) y = Level.levelHeight - height;
		
		super.update();
	}
	
	private function getAccelFromInput():Int
	{
		if (FlxG.keys.pressed.LEFT) { return -ACCEL; }
		else if (FlxG.keys.pressed.RIGHT) { return ACCEL; }
		else { return 0; }
	}
	
	private function getVelocityFromInput():Void
	{
		if (FlxG.keys.justPressed.SPACE && isTouching(FlxObject.FLOOR)) { velocity.y = JUMP; }
	}
}