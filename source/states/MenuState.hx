package states;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxDestroyUtil;
import managers.DataManager;
import managers.SoundManager;

import managers.RenderManager;
import objects.Message;
import states.PlayState;
import utils.Strings;
import objects.PlainText;

class MenuState extends FlxState
{
	private var bg:FlxText;
	private var title:PlainText;
	private var bgPt:FlxPoint = FlxPoint.get();
	private var noise:FlxSound;
	private var square:FlxSound;
	private var error:FlxSprite;
	private var entry:String = "";
	
	inline static private var PRESS_SPACE:String = "PRESS SPACE";
	
	override public function create():Void
	{
		super.create();
		
		// for debugging
		
		Global.currentLevel = 1;
		Global.levelScore = 1;
		DataManager.assignFromLevel();
		
		FlxG.autoPause = false;
		
		FlxG.camera.bgColor = FlxG.random.color();
		
		bg = new FlxText(0, 0, FlxG.width);
		bg.color = FlxG.random.color();
		bg.alpha = FlxG.random.float(0.3, 0.7);
		add(bg);
		
		for (i in 0...FlxG.random.int(20, 40))
		{
			add(new Message(Strings.junkify("MESSNGNR"), true, false));
		}
		
		title = new PlainText(0, FlxG.height - 16, PRESS_SPACE);
		title.alpha = FlxG.random.float(0.6, 0.8);
		add(title);
		
		bgPt.set(bg.x, bg.y);
		
		noise = SoundManager.playNoise();
		square = SoundManager.playSquare();
		square.pause();
		
		error = new FlxSprite();
		error.loadGraphic("images/error.png");
		error.visible = false;
		add(error);
		
		//RenderManager.setZoom(8);
		
		#if debug
		FlxG.watch.add(this, "entry");
		#end
	}
	
	override public function update():Void
	{
		if (FlxG.keys.justReleased.SPACE) { transition(); }
		
		if (FlxG.random.bool(10)) {
			bg.setPosition(FlxG.random.float(bgPt.x - 2, bgPt.x + 2), FlxG.random.float(bgPt.y - 2, bgPt.y + 2));
		}
		
		if (FlxG.random.bool(10)) { RenderManager.jiggle = !RenderManager.jiggle; }
		if (FlxG.random.bool(10)) { RenderManager.redVisible = !RenderManager.redVisible; }
		if (FlxG.random.bool(10)) { RenderManager.greenVisible = !RenderManager.greenVisible; }
		if (FlxG.random.bool(10)) { RenderManager.blueVisible = !RenderManager.blueVisible; }
		
		if (FlxG.random.bool(5)) { noise.volume = FlxG.random.float(); }
		if (FlxG.random.bool(5)) { square.volume = FlxG.random.float(0, 0.5); }
		if (FlxG.random.bool(5)) { square.playing ? square.pause() : square.resume(); }
		if (FlxG.random.bool(0.5)) { SoundManager.playMessenger(); }
		
		if (FlxG.random.bool(0.1)) { title.text = "KILL THE MESSENGER"; }
		if (FlxG.random.bool(0.1)) { title.text = "DON'T KILL THE MESSENGER"; }
		if (FlxG.random.bool(0.1)) { title.text = "THERE IS NO RECEIVER"; }
		
		if (FlxG.random.bool(25)) { error.visible = true; }
		else { error.visible = false; }
		
		bg.text = Strings.junkify(bg.text);
		
		if (FlxG.keys.justReleased.R) { FlxG.resetGame(); }
		
		if (FlxG.keys.justPressed.ENTER)
		{
			if (entry == "MESSENGER") Global.debugEnabled = true;
			else entry = "";
		}
		
		if (FlxG.keys.justPressed.ANY && !FlxG.keys.justPressed.ENTER)
		{
			entry += FlxG.keys.firstJustPressed().toString();
		}
		
		super.update();
	}
	
	private function transition():Void
	{
		RenderManager.reset(true);
		
		FlxG.switchState(new PlayState());
	}
	
	override public function destroy():Void
	{
		title = FlxDestroyUtil.destroy(title);
		
		super.destroy();
	}
}