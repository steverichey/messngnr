package managers;

import flixel.FlxG;
import flixel.system.FlxAssets;
import flixel.system.FlxSound;

class SoundManager
{
	private static inline function VOICES():Array<String>
	{
		return [ 	"sounds/msgr01",
					"sounds/msgr02",
					"sounds/msgr03",
					"sounds/msgr04",
					"sounds/msgr05",
					"sounds/msgr06" ];
	}
	
	public static function playMessenger():Void
	{
		FlxG.sound.play(FlxG.random.getObject(VOICES()) + EXT, FlxG.random.float(0.7, 0.9));
	}
	
	public static function playNoise():FlxSound
	{
		return FlxG.sound.play("sounds/noise" + EXT, FlxG.random.float(), true);
	}
	
	public static function playSquare():FlxSound
	{
		return FlxG.sound.play("sounds/noise" + EXT, FlxG.random.float(), true);
	}
	
	#if flash
	inline public static var EXT:String = ".mp3";
	#else
	inline public static var EXT:String = ".ogg";
	#end
}