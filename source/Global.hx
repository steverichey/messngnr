package;

class Global
{
	public static var debugEnabled:Bool = true;
	public static var levelScore:Int = 0;
	public static var currentLevel:Int = 1;
	public static var levelMessage:String = "I HAVE DISCOVERED SOMETHING I DID NOT EXPECT";
	public static var dooms:String = "WE ARE DOOMED";
	public static var playedOnce:Bool = false;
}