package objects;

import flixel.FlxG;
import flixel.FlxSprite;

class Sender extends FlxSprite
{
	public function new(X:Float, Y:Float)
	{
		super(X, Y);
		
		loadGraphic("images/tiles.png", true);
		animation.frameIndex = 2;
	}
}