package objects;

import flixel.FlxSprite;

class Sky extends FlxSprite
{
	public function new()
	{
		super();
		
		loadGraphic("images/sky.png");
		scrollFactor.set(0, 0);
	}
}