package objects;

import flixel.FlxG;
import flixel.text.FlxText;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxSpriteUtil;

class Message extends FlxText
{
	public function new(Text:String, NonCenter:Bool = false, Tween:Bool = true)
	{
		super(0, 0, FlxG.width, Text, 16);
		
		alignment = "center";
		scrollFactor.set(0, 0);
		
		if (!NonCenter)
		{
			borderStyle = FlxTextBorderStyle.SHADOW;
		}
		
		angle = FlxG.random.float( -15, 15);
		color = FlxG.random.color();
		alpha = FlxG.random.float(0.5, 1);
		
		FlxSpriteUtil.screenCenter(this);
		
		if (Tween)
		{
			FlxTween.tween(scale, { x:FlxG.random.float(1.1, 1.3)}, FlxG.random.float(1, 4), { ease:FlxEase.sineInOut, type:FlxTween.PINGPONG } );
			FlxTween.tween(scale, { y:FlxG.random.float(1.1, 1.3)}, FlxG.random.float(1, 4), { ease:FlxEase.sineInOut, type:FlxTween.PINGPONG } );
			FlxTween.tween(this, { angle:FlxG.random.float( -15, 15) }, FlxG.random.float(1, 4), { ease:FlxEase.sineInOut, type:FlxTween.PINGPONG } );
		}
		
		if (NonCenter)
		{
			setPosition(FlxG.random.float(-FlxG.width + 32, FlxG.width - 32), FlxG.random.float(-32, FlxG.height - 32));
		}
	}
}