package objects;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;

class ReadingMessage extends FlxSprite
{
	private var text:FlxText;
	private var close:FlxText;
	private var reading:FlxSound;
	private var dooming:Bool = false;
	private var error:FlxSprite;
	
	public static var isReading:Bool = false;
	
	public function new(Text:String)
	{
		text = new FlxText(4, 4, FlxG.width - 8, Text, 16);
		text.color = FlxColor.BLACK;
		text.scrollFactor.set(0, 0);
		
		close = new FlxText(4, FlxG.height - 12, FlxG.width - 8, "[PRESS C TO CLOSE]");
		close.color = FlxColor.BLACK;
		close.scrollFactor.set(0, 0);
		
		super(0, 0);
		makeGraphic(FlxG.width, FlxG.height, FlxColor.WHITE);
		alpha = 0.95;
		
		visible = false;
		scrollFactor.set(0, 0);
		
		reading = FlxG.sound.play("sounds/reading.mp3", 1, true);
		reading.pause();
		
		error = new FlxSprite();
		error.loadGraphic("images/error.png");
	}
	
	override public function update():Void
	{
		if (visible)
		{
			isReading = true;
			
			if (!reading.playing)
			{
				reading.resume();
			}
			
			if (FlxG.keys.justPressed.C && text.text != Global.dooms)
			{
				visible = false;
			}
			
			if (text.text == Global.dooms && !dooming)
			{
				new FlxTimer(5, function(f:FlxTimer) { Global.playedOnce = true;  FlxG.resetGame(); } );
				dooming = true;
			}
		}
		else
		{
			isReading = false;
			
			if (reading.playing)
			{
				reading.pause();
			}
		}
	}
	
	override public function draw():Void
	{
		if (visible)
		{
			super.draw();
			text.draw();
			close.draw();
			
			if (dooming && FlxG.random.bool()) error.draw();
		}
	}
}