package objects;

import flixel.FlxG;
import flixel.math.FlxPoint;
import flixel.text.FlxText;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxSpriteUtil;

import utils.Strings;

class PlainText extends FlxText
{
	private var original:String = "";
	private var spawn:FlxPoint = FlxPoint.get();
	
	public function new(X:Float = 0, Y:Float = 0, Text:String)
	{
		super(X, Y, FlxG.width, Text);
		
		alignment = "center";
		scrollFactor.set(0, 0);
		borderStyle = FlxTextBorderStyle.SHADOW;
		
		spawn.set(X, Y);
		original = Text;
	}
	
	override public function update():Void
	{
		if (FlxG.random.bool(5)) { text = Strings.junkify(text); }
		if (FlxG.random.bool(10)) { text = original; }
		
		if (FlxG.random.bool(20))
		{
			setPosition(FlxG.random.float(spawn.x - 2, spawn.x + 2), FlxG.random.float(spawn.y - 2, spawn.y + 2));
		}
		
		super.update();
	}
}