package states;
import flixel.FlxG;
import flixel.FlxState;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import managers.SoundManager;
import objects.PlainText;

import objects.Message;
import utils.Strings;
import managers.DataManager;

class WinState extends FlxState
{
	private var text:Message;
	private var press:PlainText;
	private var score:PlainText;
	private var square:FlxSound;
	private var scoreCount:Int = 0;
	
	override public function create():Void
	{
		super.create();
		
		for (i in 0...FlxG.random.int(20, 40))
		{
			add(new Message(Strings.junkify("MESSNGNR"), true, false));
		}
		
		text = new Message("MESSAGE\nRECEIVED");
		text.color = 0xffFFFFFF;
		text.alpha = 1;
		add(text);
		
		score = new PlainText(0, FlxG.height - 24, setScore());
		add(score);
		
		press = new PlainText(0, FlxG.height - 12, "PRESS SPACE");
		add(press);
		
		Global.currentLevel ++;
		
		SoundManager.playMessenger();
		DataManager.assignFromLevel();
		
		if (Global.currentLevel > 7) Global.currentLevel = 1;
		
		square = FlxG.sound.play("sounds/square.mp3", FlxG.random.float(0.01, 0.1), true);
	}
	
	override public function update():Void
	{
		if (FlxG.random.bool(1)) square.volume = FlxG.random.float(0.01, 0.1);
		
		if (scoreCount < Global.levelScore)
		{
			score.text = setScore();
		}
		
		if (FlxG.keys.justPressed.SPACE)
		{
			FlxG.switchState(new PlayState());
		}
		
		super.update();
	}
	
	private function setScore():String
	{
		if (scoreCount < Global.levelScore)
		{
			scoreCount += Std.int(Global.levelScore / 20);
		}
		
		return "LEVEL SCORE: " + scoreCount;
	}
}