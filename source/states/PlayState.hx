package states;

import flixel.effects.FlxSpriteFilter;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.math.FlxPoint;
import flixel.util.FlxDestroyUtil;
import managers.RenderManager;
import objects.PlainText;
import objects.ReadingMessage;
import objects.Sun;
import utils.Screenshot;

import objects.Player;
import objects.Level;
import objects.Sky;
import objects.Message;
import objects.Sender;
import objects.Receiver;

class PlayState extends FlxState
{
	private var sky:Sky;
	private var sun:Sun;
	private var player:Player;
	private var level:Level;
	private var message:Message;
	private var sender:Sender;
	private var receiver:Receiver;
	private var openedMessage:Bool = false;
	private var instructions:PlainText;
	private var readingMessage:ReadingMessage;
	
	override public function create():Void
	{
		super.create();
		
		sky = new Sky();
		add(sky);
		
		sun = new Sun();
		add(sun);
		
		level = new Level();
		add(level);
		
		var tileToReplace:Int = level.getTileInstances(3)[0];
		var senderPt:FlxPoint = level.getTileCoordsByIndex(tileToReplace, false);
		level.setTileByIndex(tileToReplace, 0);
		sender = new Sender(senderPt.x, senderPt.y);
		add(sender);
		
		tileToReplace = level.getTileInstances(4)[0];
		var receiverPt:FlxPoint = level.getTileCoordsByIndex(tileToReplace, false);
		level.setTileByIndex(tileToReplace, 0);
		receiver = new Receiver(receiverPt.x, receiverPt.y);
		add(receiver);
		
		player = new Player();
		add(player);
		
		message = new Message("");
		add(message);
		
		instructions = new PlainText(0, 0, "PRESS X TO OPEN MESSAGE");
		instructions.visible = false;
		add(instructions);
		
		readingMessage = new ReadingMessage(Global.levelMessage);
		add(readingMessage);
		
		FlxG.camera.setScrollBoundsRect(0, 0, level.width, level.height, true);
		FlxG.camera.follow(player);
	}
	
	override public function update():Void
	{
		FlxG.collide(player, level);
		
		if (FlxG.overlap(player, sender))
		{
			message.text = "PLEASE DELIVER\nMY MESSAGE";
			instructions.visible = true;
		}
		else
		{
			message.text = "";
		}
		
		if (FlxG.overlap(player, receiver) && instructions.visible)
		{
			if (openedMessage)
			{
				Global.levelScore = FlxG.random.int( -1, 0);
			}
			else
			{
				Global.levelScore = FlxG.random.int(100000, 999999);
			}
			
			FlxG.switchState(new WinState());
		}
		
		if (FlxG.keys.justPressed.X && !readingMessage.visible && instructions.visible)
		{
			readingMessage.visible = true;
			
			openedMessage = true;
		}
		
		super.update();
	}
	
	override public function destroy():Void
	{
		player = FlxDestroyUtil.destroy(player);
		level = FlxDestroyUtil.destroy(level);
		
		super.destroy();
	}
}