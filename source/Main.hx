package;

import flash.Lib;
import flash.display.Sprite;
import flash.events.Event;
import states.MenuState;

import flixel.FlxG;
import flixel.FlxGame;
import flixel.FlxState;

import utils.Screenshot;
import states.PlayState;
import managers.RenderManager;

class Main extends Sprite 
{
	public var gameClass:FlxGame;
	public var renderClass:RenderManager;
	
	public static function main():Void
	{	
		Lib.current.addChild(new Main());
	}
	
	public function new() 
	{
		super();
		
		if (stage != null) 
		{
			init();
		}
		else 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
	}
	
	private function init(?E:Event):Void 
	{
		if (hasEventListener(Event.ADDED_TO_STAGE))
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		gameClass = new FlxGame(200, 100, MenuState, 4, 60, 60, true, false);
		gameClass.visible = false;
		addChild(gameClass);
		
		renderClass = new RenderManager(gameClass);
		addChild(renderClass);
		
		Lib.current.stage.addEventListener(Event.ENTER_FRAME, onFrame);
	}
	
	private function onFrame(?e:Event):Void
	{
		renderClass.update();
		
		if (FlxG.keys.justReleased.R) FlxG.resetGame();
		
		if (Global.debugEnabled)
		{
			if (FlxG.keys.justReleased.J) RenderManager.jiggle = !RenderManager.jiggle;
			if (FlxG.keys.justReleased.G) RenderManager.jpegGlitch = !RenderManager.jpegGlitch;
			if (FlxG.keys.justReleased.ONE) RenderManager.redVisible = !RenderManager.redVisible;
			if (FlxG.keys.justReleased.TWO) RenderManager.greenVisible = !RenderManager.greenVisible;
			if (FlxG.keys.justReleased.THREE) RenderManager.blueVisible = !RenderManager.blueVisible;
			if (FlxG.keys.justReleased.F12) Screenshot.save(RenderManager.getScreenBytesAsPNG(), "screenshot" + FlxG.random.int(100, 999) + ".png");
		}
	}
}