package utils;

import flixel.FlxG;

class Strings
{
	static public function junkify(Value:String):String
	{
		var length:Int = Value.length;
		var insert:Bool = FlxG.random.bool();
		var target:Int = FlxG.random.int(0, length);
		var char:String = String.fromCharCode(FlxG.random.int(0, 127));
		
		if (insert)
		{
			return Value.substring(0, target) + char + Value.substring(target + 1, length);
		}
		else
		{
			return Value.substring(0, target) + char + Value.substring(target, length);
		}
	}
}