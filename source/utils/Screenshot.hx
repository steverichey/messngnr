package utils;

import openfl.utils.ByteArray;

#if flash
import flash.net.FileReference;
#end

class Screenshot
{
	/**
	 * Just a wrapper for FileReference, this opens a save file dialog in Flash.
	 * 
	 * @param	Data			The data to save, stored as a ByteArray.
	 * @param	DefaultFileName	The default name to be shown in the dialog.
	 */
	static public function save(Data:ByteArray, DefaultFileName:String):Void
	{
		#if flash
		new FileReference().save(Data, DefaultFileName);
		#end
	}
}