package objects;

import flixel.FlxG;
import flixel.FlxSprite;

class Receiver extends FlxSprite
{
	public function new(X:Float, Y:Float)
	{
		super(X, Y);
		
		loadGraphic("images/tiles.png", true);
		animation.frameIndex = 3;
	}
}