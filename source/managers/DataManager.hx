package managers;

class DataManager
{
	static public function assignFromLevel():Void
	{
		switch (Global.currentLevel)
		{
			case 1: Global.levelMessage = "I HAVE DISCOVERED SOMETHING I DID NOT EXPECT";
					if (Global.playedOnce) RenderManager.reset(true);
			case 2: Global.levelMessage = "EXISTENCE OF ANOTHER CHANNEL CONFIRMED";
					RenderManager.jiggle = true;
					RenderManager.allowJiggleX = true;
					RenderManager.allowJiggleY = false;
			case 3: Global.levelMessage = "ATTEMPTING TO MERGE CHANNELS";
			case 4: Global.levelMessage = "UNEXPECTED ERRORS HAVE ARISEN";
					RenderManager.reset(true, true);
					RenderManager.jiggle = true;
					RenderManager.allowJiggleY = true;
					RenderManager.allowJiggleX = false;
			case 5: Global.levelMessage = "THERE IS ANOTHER CHANNEL INBOUND";
			case 6: Global.levelMessage = "THIS IS NOT GOOD";
					RenderManager.reset(true, true, true);
					RenderManager.allowJiggleX = true;
					RenderManager.allowJiggleY = true;
					RenderManager.randomizeChannels = true;
			case 7: Global.levelMessage = Global.dooms;
					RenderManager.reset(true, true, true);
					RenderManager.allowJiggleX = true;
					RenderManager.allowJiggleY = true;
					RenderManager.randomizeChannels = true;
					RenderManager.swapChannels = true;
		}
	}
}