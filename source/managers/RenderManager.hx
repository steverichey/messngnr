package managers;

import openfl.geom.Point;
import openfl.Lib;
import openfl.geom.Matrix;
import openfl.display.Sprite;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.BitmapDataChannel;
import openfl.utils.ByteArray;

#if flash
import flash.display.JPEGEncoderOptions;
import flash.display.PNGEncoderOptions;
#end

import flixel.FlxG;
import flixel.FlxGame;

class RenderManager extends Sprite
{
	public static var instance:RenderManager;
	public static var jiggle:Bool = false;
	public static var allowJiggleX:Bool = true;
	public static var allowJiggleY:Bool = false;
	public static var jpegGlitch:Bool = false;
	public static var redVisible:Bool = true;
	public static var greenVisible:Bool = true;
	public static var blueVisible:Bool = true;
	public static var randomizeChannels:Bool = true;
	public static var swapChannels:Bool = false;
	
	private var cameraMap:Bitmap;
	private var renderMap:Bitmap;
	private var game:FlxGame;
	private var displayMap:Bitmap;
	private var displayMatrix:Matrix;
	private var cameraMatrix:Matrix;
	private var bytes:ByteArray = new ByteArray();
	
	private var redMap:Bitmap;
	private var blueMap:Bitmap;
	private var greenMap:Bitmap;
	
	private static var gameWidth:Int = 0;
	private static var gameHeight:Int = 0;
	private static var screenWidth:Int = 0;
	private static var screenHeight:Int = 0;
	
	public function new(GameClass:FlxGame)
	{
		super();
		
		game = GameClass;
		
		instance = this;
		
		gameWidth = FlxG.width;
		gameHeight = FlxG.height;
		
		screenWidth = Lib.current.stage.stageWidth;
		screenHeight = Lib.current.stage.stageHeight;
		
		cameraMap = new Bitmap(gameData());
		renderMap = new Bitmap(gameData());
		displayMap = new Bitmap(screenData());
		addChild(displayMap);
		
		redMap = new Bitmap(gameData());
		blueMap = new Bitmap(gameData());
		greenMap = new Bitmap(gameData());
		
		cameraMatrix = new Matrix(1 / FlxG.camera.zoom, 0, 0, 1 / FlxG.camera.zoom);
		displayMatrix = new Matrix(FlxG.camera.zoom, 0, 0, FlxG.camera.zoom);
	}
	
	public function update():Void
	{
		#if flash
		cameraMap.bitmapData = FlxG.camera.buffer;
		#else
		cameraMap.bitmapData.draw(FlxG.camera.canvas);
		#end
		
		copyChannel(renderMap, cameraMap, Channel.RED);
		copyChannel(renderMap, cameraMap, Channel.GREEN);
		copyChannel(renderMap, cameraMap, Channel.BLUE);
		
		
		renderMap.bitmapData.copyChannel(cameraMap.bitmapData, cameraMap.bitmapData.rect, zeroPoint, BitmapDataChannel.GREEN, BitmapDataChannel.GREEN);
		renderMap.bitmapData.copyChannel(cameraMap.bitmapData, cameraMap.bitmapData.rect, zeroPoint, BitmapDataChannel.BLUE, BitmapDataChannel.BLUE);
		
		renderMap.bitmapData.draw(cameraMap.bitmapData);
		displayMap.bitmapData.draw(renderMap.bitmapData, displayMatrix);
	}
	
	public static function copyChannel(Dest:Bitmap, Source:Bitmap, Ch:Channel):Void
	{
		var chan:UInt = switch (Ch)
		{
			case RED: BitmapDataChannel.RED;
			case GREEN: BitmapDataChannel.GREEN;
			case BLUE: BitmapDataChannel.BLUE;
		}
		
		Dest.bitmapData.copyChannel(Source.bitmapData, Source.bitmapData.rect, zeroPoint, chan, chan);
	}
	
	public static function clear():Void
	{
		instance.cameraMap.bitmapData = screenData();
		instance.displayMap.bitmapData = screenData();
		instance.renderMap.bitmapData = screenData();
	}
	
	public static function reset(Red:Bool = false, Green:Bool = false, Blue:Bool = false):Void
	{
		allowJiggleX = false;
		allowJiggleY = false;
		jiggle = false;
		jpegGlitch = false;
		
		redVisible = Red;
		greenVisible = Green;
		blueVisible = Blue;
		
		clear();
	}
	
	public static function getScreenBytesAsPNG():ByteArray
	{
		#if flash
		return instance.displayMap.bitmapData.encode(instance.displayMap.bitmapData.rect, new PNGEncoderOptions());
		#else
		return instance.displayMap.bitmapData.encode("png");
		#end
	}
	
	public static function screenData():BitmapData
	{
		return new BitmapData(screenWidth, screenHeight, false, 0xff000000);
	}
	
	public static function gameData():BitmapData
	{
		return new BitmapData(gameWidth, gameHeight, false, 0xff000000);
	}
	
	static private function randPt():Point
	{
		randpoint.setTo(allowJiggleX ? FlxG.random.int(-1, 1) : 0, allowJiggleY ? FlxG.random.int(-1, 1) : 0);
		
		return randpoint;
	}
	
	static private var zeroPoint:Point = new Point();
	static private var randpoint:Point = new Point();
}

enum Channel
{
	RED;
	GREEN;
	BLUE;
}