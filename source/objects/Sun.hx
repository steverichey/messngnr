package objects;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;

class Sun extends FlxSprite
{
	public function new()
	{
		super(FlxG.random.float( -16, 16), FlxG.random.float( -16, FlxG.width + 16));
		
		makeGraphic(64, 64, FlxColor.TRANSPARENT);
		
		FlxSpriteUtil.drawCircle(this, -1, -1, -1, FlxColor.RED);
		
		scrollFactor.set(0.2, 0.1);
	}
}